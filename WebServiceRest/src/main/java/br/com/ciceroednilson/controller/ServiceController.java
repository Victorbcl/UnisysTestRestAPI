package br.com.ciceroednilson.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import br.com.ciceroednilson.entity.Job;
import br.com.ciceroednilson.entity.Task;
import br.com.ciceroednilson.repository.PessoaRepository;

@Path("/service")
public class ServiceController {

	private final PessoaRepository repository = new PessoaRepository();

	@POST
	@Consumes("application/json; charset=UTF-8")
	@Path("/cadastrarJob")
	public String Cadastrar(Job job) {
		Job entity = new Job();
		try {
			entity.setName(job.getName());
			entity.setActive(job.getActive());
			entity.setTasks(job.getTasks());

			repository.Salvar(entity);

			return "Registro cadastrado com sucesso!";
		} catch (Exception e) {
			return "Erro ao cadastrar um registro " + e.getMessage();
		}
	}

	@POST
	@Consumes("application/json; charset=UTF-8")
	@Path("/cadastrarTask")
	public String CadastrarTask(Task task) {
		Task entity = new Task();
		try {
			entity.setName(task.getName());
			entity.setWeight(task.getWeight());
			entity.setCreatedAt(task.getCreatedAt());
			entity.setCompleted(task.getCompleted());

			repository.SalvarTask(entity);

			return "Registro cadastrado com sucesso!";
		} catch (Exception e) {
			return "Erro ao cadastrar um registro " + e.getMessage();
		}
	}

	@PUT
	@Produces("application/json; charset=UTF-8")
	@Consumes("application/json; charset=UTF-8")
	@Path("/alterarJob")
	public String Alterar(Job job) {
		Job entity = new Job();
		try {
			entity.setName(job.getName());
			entity.setActive(job.getActive());
			entity.setTasks(job.getTasks());

			repository.Alterar(entity);

			return "Registro alterado com sucesso!";
		} catch (Exception e) {
			return "Erro ao alterar o registro " + e.getMessage();
		}
	}

	@GET
	@Produces("application/json; charset=UTF-8")
	@Path("/listarJobs")
	public List<Job> ListarJobs() {
		return repository.ListarJobs();
	}

	@GET
	@Path("/getJob/{codigo}")
	@Produces("application/json; charset=UTF-8")
	public Job GetPessoa(@PathParam("codigo") Integer codigo) {
		return repository.GetPessoa(codigo);
	}

	@DELETE
	@Path("/excluirJob/{codigo}")
	public String Excluir(@PathParam("codigo") Integer codigo) {
		try {
			repository.Excluir(codigo);

			return "Registro excluido com sucesso!";
		} catch (Exception e) {
			return "Erro ao excluir o registro! " + e.getMessage();
		}
	}

}
