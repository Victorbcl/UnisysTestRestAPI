package br.com.ciceroednilson.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import br.com.ciceroednilson.entity.Job;
import br.com.ciceroednilson.entity.Task;

public class PessoaRepository {

	private final EntityManagerFactory entityManagerFactory;

	private final EntityManager entityManager;

	public PessoaRepository() {

		/*
		 * CRIANDO O NOSSO EntityManagerFactory COM AS PORPRIEDADOS DO ARQUIVO
		 * persistence.xml
		 */
		this.entityManagerFactory = Persistence.createEntityManagerFactory("minha-persistence-unit");

		this.entityManager = this.entityManagerFactory.createEntityManager();
	}

	/**
	 * CRIA UM NOVO REGISTRO NO BANCO DE DADOS
	 */
	public void Salvar(Job pessoaEntity) {

		this.entityManager.getTransaction().begin();
		this.entityManager.persist(pessoaEntity);

		for (Task task : pessoaEntity.getTasks()) {
			this.entityManager.persist(task);
		}

		this.entityManager.getTransaction().commit();
	}

	public void SalvarTask(Task task) {

		this.entityManager.getTransaction().begin();
		this.entityManager.persist(task);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * ALTERA UM REGISTRO CADASTRADO
	 */
	public void Alterar(Job pessoaEntity) {

		this.entityManager.getTransaction().begin();
		this.entityManager.merge(pessoaEntity);
		this.entityManager.getTransaction().commit();
	}

	/**
	 * RETORNA TODAS AS PESSOAS CADASTRADAS NO BANCO DE DADOS
	 */
	@SuppressWarnings("unchecked")
	public List<Job> ListarJobs() {

		return this.entityManager.createQuery("SELECT j FROM Job j").getResultList();
	}

	/**
	 * CONSULTA UMA PESSOA CADASTRA PELO CÓDIGO
	 */
	public Job GetPessoa(Integer codigo) {

		return this.entityManager.find(Job.class, codigo);
	}

	/**
	 * EXCLUINDO UM REGISTRO PELO CÓDIGO
	 **/
	public void Excluir(Integer codigo) {

		Job pessoa = this.GetPessoa(codigo);

		this.entityManager.getTransaction().begin();
		this.entityManager.remove(pessoa);
		this.entityManager.getTransaction().commit();

	}

}
